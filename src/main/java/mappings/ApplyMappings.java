package mappings;

import org.apache.log4j.Logger;

public class ApplyMappings {
	private final static Logger logger = Logger.getLogger(ApplyMappings.class);

	public boolean apply(final String index) {
		final ExternalProcessExecutor startZfsScript = new ExternalProcessExecutor();
		startZfsScript.addParam("/opt/reindex/reindex_app/create_mappings.sh");
		startZfsScript.addParam(index);
		final ExternalProcessResult scriptResult = startZfsScript.execute();
		logger.info("Script result:" + scriptResult.getResult());
		if (scriptResult.hasError() || scriptResult.hasException() || !scriptResult.hasResult()) {
			return false;

		}
		return true;
	}

}
