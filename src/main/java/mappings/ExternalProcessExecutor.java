package mappings;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

/**
 * Tool for calling the external processes. Migrated from KAB.
 * <p>
 * Usage example:<br />
 * <br />
 * ExtProg prog = new ExtProg();<br />
 * prog.addParam(Config.getUnzipCmd());<br />
 * prog.addParam("-Pxxx");<br />
 * prog.addParam("-o");<br />
 * prog.addParam("-d");<br />
 * prog.addParam(tmpZipDir.getAbsolutePath());<br />
 * prog.addParam(tmpZipFile.getAbsolutePath());<br />
 * prog.exec();
 * </p>
 */
public class ExternalProcessExecutor {

	protected List<String> commands = new ArrayList<String>();

	public ExternalProcessResult myResult;

	public static int MAX_STRING_SIZE = 16777215;

	public boolean ignoreErrorStream = false;

	public boolean exitOnErrorStream = true;

	public ExternalProcessExecutor() {
	}

	public void addParam(final String param) {
		if (!param.isEmpty()) {
			commands.add(param);
		}
	}

	public void clearParams() {
		commands.clear();
	}

	public ExternalProcessResult execute() {
		return execute(true);
	}

	public ExternalProcessResult execute(final boolean showDebug) {
		if (commands.isEmpty()) {
			return new ExternalProcessResult();
		}
		final String[] cmd = new String[commands.size()];
		commands.toArray(cmd);
		return execute(cmd);
	}

	public ExternalProcessResult execute(final String cmd) {
		if (StringUtils.isEmpty(cmd)) {
			myResult = new ExternalProcessResult();
			return myResult;
		}

		final String[] cmdArray = convertToStrArray(cmd);
		for (int i = 0; i < cmdArray.length; i++) {
			addParam(cmdArray[i]);
		}
		return execute(cmdArray);
	}

	protected String[] convertToStrArray(final String cmd) {
		final StringTokenizer tokens = new StringTokenizer(cmd.trim());
		final String[] cmdArray = new String[tokens.countTokens()];
		int count = 0;
		while (tokens.hasMoreTokens()) {
			cmdArray[count++] = tokens.nextToken().trim();
		}
		return cmdArray;
	}

	protected ExternalProcessResult execute(final String[] cmd) {
		return execute(cmd, true);
	}

	protected ExternalProcessResult execute(final String[] cmd, final boolean showDebug) {

		myResult = new ExternalProcessResult();
		final StringBuilder output = new StringBuilder();
		final StringBuilder error = new StringBuilder();
		ReadableByteChannel channel = null;
		InputStream bin = null;

		Process process = null;
		try {
			process = Runtime.getRuntime().exec(cmd);
			final byte[] b = new byte[1024];
			int numRead = 0;
			myResult.setProcess(process);

			bin = process.getErrorStream();

			// Assume that when there's data in the error stream,
			// an error has occurred, and returns an empty string
			if (!ignoreErrorStream && bin.available() > 0) {
				error.append(readMessageFromStream(bin));
				if (exitOnErrorStream) {
					return toExternalProcessResult(output, error);
				}
			}

			channel = Channels.newChannel(process.getInputStream());
			final ByteBuffer buf = ByteBuffer.allocate(1024);
			buf.clear();

			while ((numRead = channel.read(buf)) > -1) {
				buf.rewind();
				buf.get(b, 0, numRead);
				buf.asCharBuffer();
				final String str = new String(b, 0, numRead);
				output.append(str);

				// The following code is a fix to avoid JVM reaching the
				// 16M limit, which would cause an BufferOverflowException
				// Some of the text extraction tools may run into the
				// problem of printing out infinite number of characters.
				// The fix can also avoid this problem.
				if (output.length() > MAX_STRING_SIZE - 1024) {
					break;
				}
				buf.clear();

				// Assume that when there's data in the error stream,
				// an error has occurred, and returns an empty string
				if (!ignoreErrorStream && bin.available() > 0) {
					error.append(readMessageFromStream(bin));
					if (exitOnErrorStream) {
						return toExternalProcessResult(output, error);
					}
				}
			}

			// Read error stream again before exiting
			// Sometimes the errors took a while before they
			// are printed out, which would skip the check
			// before the while loop, and then there's no
			// output on stdout, so it never go inside the
			// while loop. When this happens, it leaves the
			// output in stderr unread.
			if (!ignoreErrorStream && bin.available() > 0) {
				error.append(readMessageFromStream(bin));
			}

		} catch (final Exception e) {
			myResult.addException(e);
		} finally {
			if (process != null) {
				process.destroy();
			}
			try {
				channel.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				bin.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return toExternalProcessResult(output, error);
	}

	protected ExternalProcessResult toExternalProcessResult(final StringBuilder output, final StringBuilder error) {
		String str = output.toString();
		myResult.setResult(str);
		str = error.toString();
		myResult.setError(str);
		return myResult;
	}

	protected String readMessageFromStream(final InputStream bin) throws IOException {
		if (bin.available() > 0) {

			final byte[] b = new byte[2048];
			final StringBuilder buf = new StringBuilder();
			final int n;
			if ((n = bin.read(b)) > -1) {
				buf.append(new String(b, 0, n));
			}
			return buf.toString();
		}
		return "";
	}

	public String getCommandLine() {
		final StringBuilder cmd = new StringBuilder();
		for (final Iterator<String> iter = commands.iterator(); iter.hasNext();) {
			final String element = iter.next();
			cmd.append(element);
			cmd.append(" ");
		}
		return cmd.toString();
	}

	public void setExitOnErrorStream(final boolean pValue) {
		exitOnErrorStream = pValue;
	}
}
