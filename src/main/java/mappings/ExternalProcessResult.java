package mappings;

import java.util.ArrayList;
import java.util.List;

/**
 * Result of {@link ExternalProcessExecutor}.
 */
public class ExternalProcessResult {

	private String result;
	private String error;
	private final List<Exception> myExceptions;
	private Process process;

	public ExternalProcessResult() {
		result = "";
		error = "";
		myExceptions = new ArrayList<Exception>();
	}

	public ExternalProcessResult(final String result, final String error) {
		this();
		this.result = result;
		this.error = error;
	}

	public void addException(final Exception pEx) {
		myExceptions.add(pEx);
	}

	public boolean hasException() {
		return myExceptions.size() > 0;
	}

	public List<Exception> getExceptions() {
		return myExceptions;
	}

	public Exception getFirstException() {
		return myExceptions.get(0);
	}

	public String getResult() {
		return result;
	}

	public String getError() {
		return error;
	}

	public boolean hasResult() {
		return result != null && result.length() > 0;
	}

	public boolean hasError() {
		return error != null && error.length() > 0;
	}

	public void setResult(final String result) {
		this.result = result != null ? result.trim() : null;
	}

	public void setError(final String error) {
		this.error = error != null ? error.trim() : null;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Standard Output Stream:\n");
		builder.append(result);
		builder.append("\nError Stream:\n");
		builder.append(error);
		return builder.toString();
	}

	public void setProcess(final Process process) {
		this.process = process;
	}

	public Process getProcess() {
		return process;
	}
}
