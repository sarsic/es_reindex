package reindex;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.log4j.Logger;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import mappings.ApplyMappings;
import reindex.destionation.DestinationElastic;
import reindex.model.BatchObject;
import reindex.settings.ReindexSettings;
import reindex.sorce.FileSourceElastic;
import reindex.sorce.SourceElastic;

/**
 * Hello world!
 *
 */
public class App {
	public static final int H_1 = 3600000;
	private final static Logger logger = Logger.getLogger(App.class);

	public static void main(final String[] args) {

		final ReindexSettings settings = new ReindexSettings();
		final JCommander jCommander = new JCommander(settings);
		boolean showUsage = false;
		logger.info("Start! " + new Date());
		try {
			jCommander.parse(args);
		} catch (final ParameterException e) {
			logger.error(e.getMessage());
			JCommander.getConsole().println(e.getMessage());
			showUsage = true;
		}
		if (showUsage || settings.isHelp()) {
			jCommander.usage();
		} else if (settings.isExceptionsRepeat()) {
			exceptionsRepeat(settings);
		} else {
			startProgram(settings);
		}
		System.exit(0);
	}

	private static void exceptionsRepeat(final ReindexSettings settings) {
		logger.info("Exceptions repeat!" + settings.getIndices());
		final BlockingQueue<BatchObject> batchObjectsQueue = new ArrayBlockingQueue<>(settings.getQueueSize());
		final Client clientSource = createSorceClient(settings);
		final RestClient clientDest = createDestClient(settings);
		// Dest thread
		final ExecutorService destThreadPool = Executors.newSingleThreadExecutor();
		destThreadPool.execute(new DestinationElastic(clientDest, batchObjectsQueue, settings.getIndices().size()));

		// Src threads
		final ExecutorService srcThreadPool = Executors.newFixedThreadPool(settings.getSourceThreadNo());
		for (final String index : settings.getIndices()) {
			srcThreadPool.execute(new FileSourceElastic(batchObjectsQueue, index, clientSource,
					settings.getResultsPerPrimaryShard()));
		}
		destThreadPool.shutdown();
		srcThreadPool.shutdown();
		while (!srcThreadPool.isTerminated() || !destThreadPool.isTerminated()) {
			logger.info("Threads not terminated");
			try {
				Thread.sleep(600000);
			} catch (final InterruptedException e) {
				logger.error(e);
			}
		}
		logger.info("Finished all threads");

	}

	private static void startProgram(final ReindexSettings settings) {
		logger.info("Kriranje niti!" + settings.getIndices());

		final BlockingQueue<BatchObject> batchObjectsQueue = new ArrayBlockingQueue<>(settings.getQueueSize());
		final Client clientSource = createSorceClient(settings);
		applyMappingsToIndices(settings.getIndices());
		final RestClient clientDest = createDestClient(settings);

		// Dest thread
		final ExecutorService destThreadPool = Executors.newSingleThreadExecutor();
		destThreadPool.execute(new DestinationElastic(clientDest, batchObjectsQueue, settings.getIndices().size()));

		// Src threads
		final ExecutorService srcThreadPool = Executors.newFixedThreadPool(settings.getSourceThreadNo());
		for (final String index : settings.getIndices()) {
			srcThreadPool.execute(
					new SourceElastic(batchObjectsQueue, index, clientSource, settings.getResultsPerPrimaryShard()));
		}
		destThreadPool.shutdown();
		srcThreadPool.shutdown();
		while (!srcThreadPool.isTerminated() || !destThreadPool.isTerminated()) {
			logger.info("Threads not terminated");
			try {
				Thread.sleep(600000);
			} catch (final InterruptedException e) {
				logger.error(e);
			}
		}
		logger.info("Finished all threads");

	}

	private static void applyMappingsToIndices(final List<String> indices) {
		final ApplyMappings applyMappings = new ApplyMappings();
		for (final String index : indices) {
			if (!applyMappings.apply(index)) {
				throw new IllegalStateException(index + " not createt abort opperation");
			}
		}

	}

	private static Client createSorceClient(final ReindexSettings settings) {
		final ImmutableSettings.Builder settingsBuilder = ImmutableSettings.settingsBuilder();
		settingsBuilder.put("cluster.name", settings.getSourceClusterName());
		settingsBuilder.put("client.transport.ping_timeout", "240s");
		final InetSocketTransportAddress address = new InetSocketTransportAddress(settings.getSourceHost(),
				settings.getSourcePort());
		final Client clientSource = new TransportClient(settingsBuilder).addTransportAddress(address);
		return clientSource;
	}

	private static RestClient createDestClient(final ReindexSettings settings) {
		final RestClient clientDest = RestClient.builder(new HttpHost(settings.getDestHost(), settings.getDestPort()))
				.setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
					@Override
					public RequestConfig.Builder customizeRequestConfig(
							final RequestConfig.Builder requestConfigBuilder) {
						return requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(H_1);
					}
				}).setMaxRetryTimeoutMillis(H_1).build();
		return clientDest;
	}

}
