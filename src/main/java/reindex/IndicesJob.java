package reindex;

import java.util.List;

public abstract class IndicesJob implements Runnable {

	private final List<String> indices;

	public IndicesJob(final List<String> indices) {
		this.indices = indices;
	}

	public void run() {
		for (final String index : indices) {
			submitIndex(index);
		}
	}

	public abstract void submitIndex(final String index);

}
