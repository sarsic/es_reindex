package reindex.destionation;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.log4j.Logger;
import org.elasticsearch.client.RestClient;

import reindex.model.BatchObject;
import reindex.model.SourceText;

public class DestinationElastic implements Runnable {
	private static final String INDEX_S_BATCH_S_INDEXED_IN_SS = "Index:%s batch:%s Indexed in %ss";
	private final static Logger logger = Logger.getLogger(DestinationElastic.class);

	public static final String EXIT = "exit";
	private final BlockingQueue<BatchObject> batchObjectsQueue;

	private static final String S_S_BULK = "/%s/%s/_bulk";
	private static final String COMMA = ",";
	private static final String POST = "POST";
	private static final int MAX_NUMBER_OF_ATTEMPTS = 5;
	public static String MESSAGE_TYPE = "message";
	public static String ENGLISH_STEMMER = "_english_stemmer";
	private final RestClient clientDest;
	private int exitCount = 0;
	private final int idicesNO;

	public DestinationElastic(final RestClient clientDest, final BlockingQueue<BatchObject> batchObjectsQueue,
			final int idicesNO) {
		this.clientDest = clientDest;
		this.batchObjectsQueue = batchObjectsQueue;
		this.idicesNO = idicesNO;
	}

	public void index(final SourceText sourceText, final String index) {
		final Map<String, String> params = Collections.emptyMap();
		final HttpEntity entity = new NStringEntity(sourceText.getSource(), ContentType.APPLICATION_JSON);

		final boolean indexed = indexBulk(index, params, entity, 0);
		if (!indexed) {
			try {
				logger.info("Writting ids to file");
				final File exceptionsFile = new File("/opt/reindex/reindex_app/exceptions" + index + ".txt");
				if (!exceptionsFile.exists()) {
					exceptionsFile.createNewFile();
				}
				final String commaSep = String.join(COMMA, sourceText.getIds()) + COMMA;
				Files.write(exceptionsFile.toPath(), commaSep.getBytes(), StandardOpenOption.APPEND);
			} catch (final Exception e) {
				logger.error(e);
			}
		}

	}

	private boolean indexBulk(final String index, final Map<String, String> params, final HttpEntity entity,
			final int attempt) {
		try {
			clientDest.performRequest(POST, String.format(S_S_BULK, index, MESSAGE_TYPE), params, entity);
			// final RequestLine requestLine = response.getRequestLine();
			// statusCode = response.getStatusLine().getStatusCode();
			return true;
		} catch (final Exception e) {
			logger.error(e);
			if (attempt < MAX_NUMBER_OF_ATTEMPTS) {
				try {
					logger.error("Exception: " + attempt + " sleeping for : " + (attempt + 1) * 10 + "s");
					Thread.sleep((attempt + 1) * 10000L);
				} catch (final InterruptedException e1) {
					logger.error(e);
				}
				return indexBulk(index, params, entity, attempt + 1);
			}
		}
		return false;
	}

	@Override
	public void run() {
		// consuming messages until exit message is received
		try {
			logger.info("My first sleep 10s");
			Thread.sleep(10000);
		} catch (final InterruptedException e1) {
			logger.error(e1);
		}
		try {
			while (true) {
				Thread.sleep(10);
				final BatchObject msg = batchObjectsQueue.take();
				if (msg.getIndex().equals(EXIT)) {
					exitCount++;
					if (exitCount == idicesNO) {
						break;
					}
					continue;
				}
				final long start = System.currentTimeMillis();
				index(msg.getSorce(), msg.getIndex());
				final long end = System.currentTimeMillis() - start;
				logger.info(String.format(INDEX_S_BATCH_S_INDEXED_IN_SS, msg.getIndex(), msg.getSize(),
						TimeUnit.MILLISECONDS.toSeconds(end)));
			}
			logger.info("Closing destination thread.");
		} catch (final InterruptedException e) {
			logger.error(e);
		}

	}

}
