package reindex.model;

public class BatchObject {
	private final SourceText sorce;
	private final String index;
	private final int size;

	public BatchObject(final int size, final SourceText sorce, final String index) {
		this.size = size;
		this.sorce = sorce;
		this.index = index;
	}

	public SourceText getSorce() {
		return sorce;
	}

	public String getIndex() {
		return index;
	}

	public int getSize() {
		return size;
	}
}
