package reindex.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Contains all fields which are found in mapping.json file and it is used to
 * generate JSON file which is going to be indexed. So care must be taken in
 * order to keep values in this enumeration in sync with mapping.json.
 */
public enum MessageIndexField {

	ATTACHMENT,

	NON_STANDARD_HEADER,

	BCC(MimeHeaderName.BCC),

	BCC_SIZE,

	BODY_SIZE,

	BODY_TEXT,

	BODY_TYPE,

	BODY_SPECIAL_VALUES,

	CC(MimeHeaderName.CC),

	CC_SIZE,

	FROM(MimeHeaderName.FROM),

	FROM_SIZE,

	HIDDEN_RECIPIENTS(MimeHeaderName.HIDDEN_RECIPIENTS),

	HIDDEN_RECIPIENTS_SIZE,

	MESSAGE_ID(MimeHeaderName.MESSAGE_ID),

	JATHEON_DUPLICATES,

	PROCESSED_DATE(MimeHeaderName.X_JATHEON_PROCESSED_DATE),

	PROCESSED_TIME(MimeHeaderName.X_JATHEON_PROCESSED_DATE),

	PROCESSED_DATETIME,

	JATHEON_ARCHIVED_DATETIME,

	REPLY_TO(MimeHeaderName.REPLY_TO),

	REPLY_TO_SIZE,

	REFERENCES(MimeHeaderName.REFERENCES),

	THREAD_INDEX_STRIPPED(MimeHeaderName.THREAD_INDEX_STRIPPED),

	SENDER(MimeHeaderName.SENDER),

	SENDER_SIZE,

	SENT_DATE(MimeHeaderName.DATE),

	SENT_TIME(MimeHeaderName.DATE),

	SUBJECT(MimeHeaderName.SUBJECT),

	SUBJECT_SPECIAL_VALUES,

	SUBJECT_STRIPPED(MimeHeaderName.SUBJECT_STRIPPED),

	TO(MimeHeaderName.TO),

	TO_SIZE,

	ORIGINAL_SIZE,

	ARCHIVED_SIZE,

	DIRECTION,

	FILE_PATH,

	HASH,

	RULES,

	HAS_COMMENTS,

	RECIPIENTS_SIZE;

	/**
	 * Contains all fields which are found in mapping.json non standard headers
	 * object
	 */
	public enum NonStandardHeader {
		HEADER_NAME,

		HEADER_VALUE;

		public String getLowerCaseName() {
			return name().toLowerCase();
		}

		public String getPath() {
			return NON_STANDARD_HEADER.getLowerCaseName() + DOT + getLowerCaseName();
		}
	}

	public static final String DOT = ".";

	/**
	 * Contains all fields which are found in mapping.json attachment object
	 */
	public enum Attachment {

		DATE,

		ATTACHMENT_NAME,

		ATTACHMENT_NAME_SPECIAL_VALUES,

		SIZE,

		CONTENT_TYPE,

		ENCODED,

		CONTENT,

		CONTENT_SPECIAL_VALUES,

		PATH,

		ARCHIVED_FILENAME;

		public static final Map<String, String> SPECIAL_TO_ORIGINAL_PATH_MAP = new HashMap<String, String>();
		static {
			SPECIAL_TO_ORIGINAL_PATH_MAP.put(ATTACHMENT_NAME_SPECIAL_VALUES.getPath(), ATTACHMENT_NAME.getPath());
			SPECIAL_TO_ORIGINAL_PATH_MAP.put(CONTENT_SPECIAL_VALUES.getPath(), CONTENT.getPath());
		}

		public static final Map<String, String> SPECIAL_PATH_TO_SPECIAL_MAP = new HashMap<String, String>();
		static {
			SPECIAL_PATH_TO_SPECIAL_MAP.put(ATTACHMENT_NAME_SPECIAL_VALUES.getPath(),
					ATTACHMENT_NAME_SPECIAL_VALUES.getLowerCaseName());
			SPECIAL_PATH_TO_SPECIAL_MAP.put(CONTENT_SPECIAL_VALUES.getPath(),
					CONTENT_SPECIAL_VALUES.getLowerCaseName());
		}

		public String getLowerCaseName() {
			return name().toLowerCase();
		}

		public String getEnglishStemmerVersion() {
			return getPath() + "_english_stemmer";
		}

		public String getPath() {
			return ATTACHMENT.getLowerCaseName() + DOT + getLowerCaseName();
		}
	}

	/**
	 * Contains MIME header names which are related to this JSON field. E.g.
	 * MESSAGE_ID("message-id") JSON field should be filled with value of MIME
	 * header "message-id".
	 */
	private final String[] mimeHeaderNames;

	MessageIndexField(final String... mimeHeaderNames) {
		this.mimeHeaderNames = mimeHeaderNames;
	}

	public String[] getMimeHeaderNames() {
		return mimeHeaderNames;
	}

	public String getLowerCaseName() {
		return name().toLowerCase();
	}

	public static MessageIndexField fromLowerCaseName(final String lowerCaseName) {
		return valueOf(lowerCaseName.toUpperCase());
	}

	public String getEnglishStemmerVersion() {
		return getLowerCaseName() + DOT + getLowerCaseName() + "_english_stemmer";
	}

	/**
	 * For specified {@link MessageIndexField} get {@link MessageIndexField}
	 * which represents size field for it.
	 */
	public static MessageIndexField getSizeField(final MessageIndexField field) {
		switch (field) {
		case TO:
			return TO_SIZE;
		case CC:
			return CC_SIZE;
		case BCC:
			return BCC_SIZE;
		case HIDDEN_RECIPIENTS:
			return HIDDEN_RECIPIENTS_SIZE;
		case REPLY_TO:
			return REPLY_TO_SIZE;
		case FROM:
			return FROM_SIZE;
		case SENDER:
			return SENDER_SIZE;
		default:
			throw new IllegalArgumentException("There is now size field for: " + field);
		}
	}

	public static Set<String> ATTACHMENT_FIELDS = new HashSet<String>();
	static {
		for (final Attachment field : Attachment.values()) {
			ATTACHMENT_FIELDS.add(field.getPath());
		}
	}

	public static Set<String> NON_STANDARD_HEADERS_FIELDS = new HashSet<String>();
	static {
		for (final NonStandardHeader field : NonStandardHeader.values()) {
			NON_STANDARD_HEADERS_FIELDS.add(field.getPath());
		}
	}
}
