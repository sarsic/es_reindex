package reindex.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Constants for MIME message header names.
 */
public final class MimeHeaderName {

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_LENGTH = "Content-Length";
	public static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	public static final String CONTENT_ID = "Content-ID";
	public static final String CONTENT_MD5 = "Content-MD5";
	public static final String CONTENT_DESCRIPTION = "Content-Description";
	public static final String CONTENT_LANGUAGE = "Content-Language";
	public static final String CONTENT_LOCATION = "Content-Location";
	public static final String CONTENT_IDENTIFIER = "Content-Identifier";
	public static final String CONTENT_IDENTIFER = "Content-Identifer";

	public static final String MIME_VERSION = "MIME-Version";
	public static final String DATE = "Date";
	public static final String MESSAGE_ID = "Message-ID";
	public static final String SUBJECT = "Subject";
	public static final String SUBJECT_STRIPPED = "X-Jatheon-subject-stripped";

	public static final String FROM = "From";
	public static final String SENDER = "Sender";
	public static final String TO = "To";
	public static final String CC = "Cc";
	public static final String BCC = "Bcc";
	public static final String REPLY_TO = "Reply-To";
	public static final String RECIPIENT = "Recipient";
	public static final String RECIPIENTS = "Recipients";
	public static final String RECEIVED = "Received";
	public static final String RETURN_PATH = "Return-Path";
	public static final String IN_REPLY_TO = "In-Reply-To";
	public static final String REFERENCES = "References";
	public static final String SUPERSEDES = "Supersedes";
	public static final String THREAD_INDEX = "Thread-Index";
	// Field used for Microsoft threading, contains first 27 characters of
	// {@link #THREAD_INDEX} field.
	public static final String THREAD_INDEX_STRIPPED = "X-Jatheon-thread-index-stripped";

	public static final String RESENT_DATE = "Resent-Date";

	public static final String RESENT_FROM = "Resent-From";
	public static final String RESENT_SENDER = "Resent-Sender";
	public static final String RESENT_TO = "Resent-To";
	public static final String RESENT_CC = "Resent-Cc";
	public static final String RESENT_BCC = "Resent-Bcc";

	public static final String X_JATHEON_SMTP_RECIPIENTS = "X-Jatheon-Smtp-Recipients";
	public static final String X_JATHEON_SMTP_SENDER = "X-Jatheon-Smtp-Sender";
	public static final String X_JATHEON_JRN_RECIPIENTS = "X-Jatheon-Jrn-Recipients";
	public static final String X_JATHEON_JRN_SENDER = "X-Jatheon-Jrn-Sender";
	public static final String X_JATHEON_POP_USER = "X-Jatheon-Pop-User";
	public static final String X_JATHEON_RCPT_TO = "X-Jatheon-Rcpt-To";
	public static final String X_RCPT_TO = "X-Rcpt-To";
	public static final String X_JATHEON_JRN_TYPE = "X-Jatheon-Jrn-Type";
	public static final String X_JATHEON_MAIL_FROM = "X-Jatheon-Mail-From";
	public static final String X_MAIL_FROM = "X-Mail-From";
	public static final String X_JATHEON_DUPLICATE_RECIPIENTS = "X-Jatheon-Duplicate-Recipients";
	public static final String X_JATHEON_DUPLICATE_RECIPIENTS_BCC = "X-Jatheon-Duplicate-Recipients-Bcc";
	public static final String X_JATHEON_DUPLICATE_SENDER = "X-Jatheon-Duplicate-Sender";

	public static final String X_JATHEON_MAIL_TYPE = "X-Jatheon-Mail-Type";
	public static final String X_JATHEON_IM_TYPE = "X-Jatheon-Im-Type";

	public static final String X_JATHEON_PROCESSED_DATE = "X-Jatheon-Processed-Date";

	public static final String X_MS_JOURNAL_REPORT = "X-MS-Journal-Report";

	public static final String X_JATHEON_PREFIX = "X-Jatheon-";
	public static final String X_JATHEON_JRN_PREFIX = "X-Jatheon-Jrn-";

	public static final String X_JATHEON_PST_SOURCE = "X-Jatheon-PST-Source";
	public static final String X_JATHEON_EML_INGESTION = "X-Jatheon-EML-Ingestion";

	public static final String X_GWJ_FROM = "X-GWJ-From";
	public static final String X_GWJ_TO = "X-GWJ-To";
	public static final String X_GWJ_CC = "X-GWJ-Cc";
	public static final String X_GWJ_BCC = "X-GWJ-Bcc";
	public static final String X_GWJ_DATE = "X-GWJ-Date";
	public static final String X_GWJ_MESSAGE_ID = "X-GWJ-Message-ID";

	public static final String X_JATHEON_SOCIAL_EMAIL = "X-Jatheon-Social-Email";

	public static final String X_MS_EXCHANGE_ORGANIZATION_BCC = "X-MS-Exchange-Organization-BCC";

	public static final String X_JATHEON_SENDER = "X-Jatheon-Sender";
	public static final String X_JATHEON_MSGTYPE = "X-Jatheon-MsgType";
	/**
	 * Hidden recipients header that is used for export (eml export,pst
	 * export...)
	 */
	public static final String HIDDEN_RECIPIENTS_EXPORT = "Hidden-recipients";
	public final static List<String> JATHEON_HIDDEN_EXPORT_HEADERS = new ArrayList<String>();
	static {
		JATHEON_HIDDEN_EXPORT_HEADERS.add(MimeHeaderName.X_JATHEON_DUPLICATE_RECIPIENTS);
		JATHEON_HIDDEN_EXPORT_HEADERS.add(MimeHeaderName.X_JATHEON_DUPLICATE_RECIPIENTS_BCC);
	}

	// following headers will contain info about final recipient in mailing list
	// messages
	public static final String X_ORIGINAL_TO = "X-Original-To";
	public static final String X_ENVELOPE_TO = "X-Envelope-To";
	public static final String ENVELOPE_TO = "Envelope-To";
	public static final String X_DELIVERED_TO = "X-Delivered-To";
	public static final String DELIVERED_TO = "Delivered-To";

	// mailing lists headers
	public static final String LIST_ID = "List-ID";
	public static final String LIST_OWNER = "List-Owner";
	public static final String LIST_POST = "List-Post";
	public static final String LIST_SUBSCRIBE = "List-Subscribe";
	public static final String LIST_UNSUBSCRIBE = "List-Unsubscribe";
	public static final String LIST_HELP = "List-Help";
	public static final String LIST_ARCHIVE = "List-Archive";

	public static final String JATHEON_RULES_FORWARD_ACTION_MAIL_HEADER = "JATHEON-RULE-FORWARD-ACTION";

	public static final String[] HIDDEN_RECIPIENTS = { X_JATHEON_SMTP_RECIPIENTS, X_JATHEON_JRN_RECIPIENTS,
			X_JATHEON_POP_USER, X_JATHEON_RCPT_TO, X_GWJ_BCC, X_RCPT_TO, X_JATHEON_DUPLICATE_RECIPIENTS };

	public static final String[] HIDDEN_RECIPIENTS_FOR_MAILING_LIST = { X_ORIGINAL_TO, X_ENVELOPE_TO, ENVELOPE_TO,
			X_DELIVERED_TO, DELIVERED_TO };

	public static final String[] SENDERS = { SENDER, X_JATHEON_SMTP_SENDER, X_JATHEON_JRN_SENDER, X_JATHEON_MAIL_FROM,
			X_JATHEON_DUPLICATE_SENDER, X_MAIL_FROM };

	public static final String[] CLEAN_HASH_HEADERS = { FROM, TO, CC, SUBJECT, DATE };

	public static final String[] CHECKSUM_AVOID_HEADERS = { X_JATHEON_DUPLICATE_RECIPIENTS, X_JATHEON_DUPLICATE_SENDER,
			MESSAGE_ID };

	public static final String[] LIST_HEADERS = { LIST_ID, LIST_OWNER, LIST_POST, LIST_SUBSCRIBE, LIST_UNSUBSCRIBE,
			LIST_HELP, LIST_ARCHIVE };

	/**
	 * Bcc fields to exclude when exporting message. Aside from standard 'Bcc'
	 * fields, Microsoft introduces it's own fields for this purpose. We'll try
	 * to list them here for exclusion.
	 */
	public static final String[] BCC_EXPORT_REMOVE_FIELDS = { BCC, X_MS_EXCHANGE_ORGANIZATION_BCC };

	private MimeHeaderName() {
		// only constant's here, no need to initialize
	}
}
