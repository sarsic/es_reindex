package reindex.model;

import org.apache.commons.lang3.ArrayUtils;

import reindex.model.MessageIndexField.Attachment;
import reindex.model.MessageIndexField.NonStandardHeader;

public class SearchHeaders {

	public static final String[] INCLUDED_FIELDS = { MessageIndexField.TO.getLowerCaseName(),
			MessageIndexField.CC.getLowerCaseName(), MessageIndexField.BCC.getLowerCaseName(),
			MessageIndexField.HIDDEN_RECIPIENTS.getLowerCaseName(), MessageIndexField.BODY_TEXT.getLowerCaseName(),

			MessageIndexField.BODY_TYPE.getLowerCaseName(), MessageIndexField.BODY_SPECIAL_VALUES.getLowerCaseName(),
			MessageIndexField.SENDER.getLowerCaseName(), MessageIndexField.FROM.getLowerCaseName(),
			MessageIndexField.REPLY_TO.getLowerCaseName(), MessageIndexField.MESSAGE_ID.getLowerCaseName(),
			MessageIndexField.REFERENCES.getLowerCaseName(), MessageIndexField.PROCESSED_DATE.getLowerCaseName(),
			MessageIndexField.PROCESSED_TIME.getLowerCaseName(), MessageIndexField.SENT_DATE.getLowerCaseName(),
			MessageIndexField.SENT_TIME.getLowerCaseName(), MessageIndexField.SUBJECT.getLowerCaseName(),

			MessageIndexField.SUBJECT_SPECIAL_VALUES.getLowerCaseName(),
			MessageIndexField.SUBJECT_STRIPPED.getLowerCaseName(), MessageIndexField.FILE_PATH.getLowerCaseName(),
			MessageIndexField.HAS_COMMENTS.getLowerCaseName(), MessageIndexField.ORIGINAL_SIZE.getLowerCaseName(),
			MessageIndexField.ARCHIVED_SIZE.getLowerCaseName(), MessageIndexField.HASH.getLowerCaseName(),
			MessageIndexField.RULES.getLowerCaseName(), MessageIndexField.DIRECTION.getLowerCaseName(),

			MessageIndexField.Attachment.ATTACHMENT_NAME.getPath(),
			MessageIndexField.Attachment.ATTACHMENT_NAME_SPECIAL_VALUES.getPath(),
			MessageIndexField.Attachment.PATH.getPath(), MessageIndexField.Attachment.ARCHIVED_FILENAME.getPath(),
			MessageIndexField.Attachment.CONTENT_SPECIAL_VALUES.getPath(), MessageIndexField.Attachment.SIZE.getPath(),
			MessageIndexField.Attachment.ENCODED.getPath(), MessageIndexField.Attachment.CONTENT_TYPE.getPath() };

	public static final String[] INCLUDED_OTHER_FIELDS = { MessageIndexField.BCC_SIZE.getLowerCaseName(),
			MessageIndexField.BODY_SIZE.getLowerCaseName(), MessageIndexField.CC_SIZE.getLowerCaseName(),
			MessageIndexField.FROM_SIZE.getLowerCaseName(), MessageIndexField.HIDDEN_RECIPIENTS_SIZE.getLowerCaseName(),
			MessageIndexField.JATHEON_DUPLICATES.getLowerCaseName(),
			MessageIndexField.PROCESSED_DATETIME.getLowerCaseName(),
			MessageIndexField.JATHEON_ARCHIVED_DATETIME.getLowerCaseName(),
			MessageIndexField.REPLY_TO_SIZE.getLowerCaseName(),
			MessageIndexField.THREAD_INDEX_STRIPPED.getLowerCaseName(),
			MessageIndexField.SENDER_SIZE.getLowerCaseName(), MessageIndexField.TO_SIZE.getLowerCaseName(),
			MessageIndexField.RECIPIENTS_SIZE.getLowerCaseName(), MessageIndexField.Attachment.DATE.getPath(),
			MessageIndexField.Attachment.CONTENT.getPath(), MessageIndexField.BODY_TEXT.getEnglishStemmerVersion(),
			MessageIndexField.SUBJECT.getEnglishStemmerVersion(),
			MessageIndexField.Attachment.CONTENT.getEnglishStemmerVersion(),
			MessageIndexField.Attachment.ATTACHMENT_NAME.getEnglishStemmerVersion() };

	public static final String[] INCLUDED_FIELDS_ALL = ArrayUtils.addAll(INCLUDED_OTHER_FIELDS,
			ArrayUtils.addAll(INCLUDED_FIELDS, MessageIndexField.NonStandardHeader.HEADER_NAME.getPath(),
					MessageIndexField.NonStandardHeader.HEADER_VALUE.getPath()));
}
