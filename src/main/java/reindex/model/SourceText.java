package reindex.model;

import java.util.List;

public class SourceText {
	private final String source;
	private final List<String> ids;

	public SourceText(final String source, final List<String> ids) {
		this.source = source;
		this.ids = ids;
	}

	public List<String> getIds() {
		return ids;
	}

	public String getSource() {
		return source;
	}

}
