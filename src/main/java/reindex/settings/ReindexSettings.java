package reindex.settings;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class ReindexSettings {

	@Parameter(names = { "-h", "--help" }, description = "show help", help = true)
	private boolean help;

	@Parameter(names = { "-i", "--indices" }, description = "indices that should be reindexed")
	private final List<String> indices = new ArrayList<>();

	@Parameter(names = { "-b", "--batch" }, description = "batch size ")
	public int batchSize = 50;

	@Parameter(names = { "-st", "--sourceThreadNo" }, description = "Source thread no ")
	public int sourceThreadNo = 2;

	@Parameter(names = { "-qs", "--queueSize" }, description = "Queue size")
	public int queueSize = 2;

	@Parameter(names = { "-sc", "--sourceClusterName" }, description = "Source clustern name ")
	public String sourceClusterName = "ergocluster";

	@Parameter(names = { "-sho", "--sourceHost" }, description = "Source host name ")
	public String sourceHost = "localhost";

	@Parameter(names = { "-sp", "--sourcePort" }, description = "Source port number")
	public int sourcePort = 1;

	@Parameter(names = { "-dho", "--destHost" }, description = "Destination host name ")
	public String destHost = "localhost";

	@Parameter(names = { "-r",
			"--resultsPerPrimaryShard" }, description = "number of results that search query should return per primary shard")
	public int resultsPerPrimaryShard = 100;

	@Parameter(names = { "-dp", "--destPort" }, description = "Destination port number")
	public int destPort = 1;

	@Parameter(names = { "-e", "--exceptionsRepeat" }, arity = 1, description = "Exceptions repeat")
	public boolean exceptionsRepeat;

	public boolean isHelp() {
		return help;
	}

	public boolean isExceptionsRepeat() {
		return exceptionsRepeat;
	}

	public String getSourceClusterName() {
		return sourceClusterName;
	}

	public String getSourceHost() {
		return sourceHost;
	}

	public int getSourcePort() {
		return sourcePort;
	}

	public int getQueueSize() {
		return queueSize;
	}

	public List<String> getIndices() {
		return indices;
	}

	public String getDestHost() {
		return destHost;
	}

	public int getDestPort() {
		return destPort;
	}

	public int getSourceThreadNo() {
		return sourceThreadNo;
	}

	public int getResultsPerPrimaryShard() {
		return resultsPerPrimaryShard;
	}

	public int getBatchSize() {
		return batchSize;
	}
}
