package reindex.sorce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import reindex.destionation.DestinationElastic;
import reindex.model.BatchObject;
import reindex.model.MessageIndexField;
import reindex.model.SearchHeaders;
import reindex.model.SourceText;

public class SourceElastic implements Runnable {
	private final static Logger logger = Logger.getLogger(SourceElastic.class);
	public static String MESSAGE_TYPE = "message";
	public static String ENGLISH_STEMMER = "_english_stemmer";
	private static final String SUBJECT_STEMMER_NEW = MessageIndexField.SUBJECT.getLowerCaseName() + ENGLISH_STEMMER;
	private static final String SUBJECT_STEMMER_OLD = MessageIndexField.SUBJECT.getEnglishStemmerVersion();

	private static final String BODY_STEMMER_NEW = MessageIndexField.BODY_TEXT.getLowerCaseName() + ENGLISH_STEMMER;
	private static final String BODY_STEMMER_OLD = MessageIndexField.BODY_TEXT.getEnglishStemmerVersion();
	private static final String ACTION_META = "{ \"index\" : { \"_index\" : \"%s\", \"_type\" : \"%s\",\"_id\" : \"%s\" } }%n";
	private static final String NEW_LINE = "\n";
	private static final String T = "T";

	private static final String RETRIEVED = " Retrieved: %s/%s";
	private final String index;
	private final Client clientSource;
	private final int resultPerPrimaryShard;
	private final BlockingQueue<BatchObject> batchObjectsQueue;

	public SourceElastic(final BlockingQueue<BatchObject> batchObjectsQueue, final String index,
			final Client clientSource, final int resultPerPrimaryShard) {
		this.index = index;
		this.clientSource = clientSource;
		this.resultPerPrimaryShard = resultPerPrimaryShard;
		this.batchObjectsQueue = batchObjectsQueue;
	}

	private void fetch() {
		SearchResponse scrollResponse = getScrollInterval(clientSource,
				getInitialScrollId(clientSource, index, resultPerPrimaryShard));
		final long totalHitCount = scrollResponse.getHits().getTotalHits();
		int hitCount = 0;
		logger.info("Found " + totalHitCount + " items for reindex.");

		while (scrollResponse.getHits().getHits().length > 0) {
			hitCount += scrollResponse.getHits().getHits().length;
			putQueue(new BatchObject(hitCount, prepareHitsForIndex(index, scrollResponse), index));

			final long start = System.currentTimeMillis();
			scrollResponse = getScrollInterval(clientSource, scrollResponse.getScrollId());
			final long end = System.currentTimeMillis() - start;
			logger.info("Index:" + index + String.format(RETRIEVED, hitCount, totalHitCount) + " in "
					+ TimeUnit.MILLISECONDS.toSeconds(end) + "s");
		}
		putQueue(new BatchObject(0, null, DestinationElastic.EXIT));
		logger.info("Finished index: " + index);
	}

	private void putQueue(final BatchObject batchObject) {
		try {
			batchObjectsQueue.put(batchObject);
		} catch (final InterruptedException e) {
			logger.error(e);
		}
	}

	private SourceText prepareHitsForIndex(final String index, final SearchResponse scrollResponse) {
		final SearchHit[] searchHits = scrollResponse.getHits().getHits();
		final StringBuilder indexStringBuilder = new StringBuilder();
		final List<String> ids = new ArrayList<>();
		for (final SearchHit searchHit : searchHits) {
			try {
				final Map<String, Object> sourceMap = searchHit.getSource();
				if (sourceMap.get(MessageIndexField.PROCESSED_DATETIME.getLowerCaseName()) == null) {
					final String dateTime = sourceMap.get(MessageIndexField.PROCESSED_DATE.getLowerCaseName()) + T
							+ sourceMap.get(MessageIndexField.PROCESSED_TIME.getLowerCaseName());
					sourceMap.put(MessageIndexField.PROCESSED_DATETIME.getLowerCaseName(), dateTime);
				}
				// subject
				sourceMap.put(SUBJECT_STEMMER_NEW, sourceMap.get(SUBJECT_STEMMER_OLD));
				sourceMap.remove(SUBJECT_STEMMER_OLD);
				// body
				sourceMap.put(BODY_STEMMER_NEW, sourceMap.get(BODY_STEMMER_OLD));
				sourceMap.remove(BODY_STEMMER_OLD);

				final XContentBuilder builder = XContentFactory.contentBuilder(Requests.INDEX_CONTENT_TYPE)
						.map(sourceMap);
				final String actionMetaData = String.format(ACTION_META, index, MESSAGE_TYPE, searchHit.getId());
				indexStringBuilder.append(actionMetaData);
				indexStringBuilder.append(builder.string());
				indexStringBuilder.append(NEW_LINE);
				ids.add(searchHit.getId());
			} catch (final IOException e) {
				logger.error(e);
			}
		}
		return new SourceText(indexStringBuilder.toString(), ids);
	}

	private String getInitialScrollId(final Client clientSource, final String index, final int resultPerPrimaryShard) {
		return clientSource.prepareSearch(index).setTypes(MESSAGE_TYPE).setQuery(getQuery())
				.setSearchType(SearchType.SCAN).setScroll(TimeValue.timeValueHours(1)).setSize(resultPerPrimaryShard)
				.setFetchSource(SearchHeaders.INCLUDED_FIELDS_ALL, null).execute().actionGet().getScrollId();
	}

	protected MatchAllQueryBuilder getQuery() {
		return QueryBuilders.matchAllQuery();
	}

	private SearchResponse getScrollInterval(final Client clientSource, final String scrollId) {
		return clientSource.prepareSearchScroll(scrollId).setScroll(TimeValue.timeValueHours(1)).execute().actionGet();
	}

	@Override
	public void run() {
		fetch();
	}

}
